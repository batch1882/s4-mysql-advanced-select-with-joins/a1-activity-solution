-- Activity
-- 1. Create a solutions.sql file inside s04/a1 project and do the following using the music_db database:

-- a.  Find all artists that has letter d in it's name.
SELECT * FROM artists WHERE name LIKE "%d%";

-- b. Find all songs that has a length of less than 230.
SELECT * FROM songs WHERE length < 230;

-- c. Join the 'albums' and 'songs' tables. (Only show the album name, song name, and song length)

SELECT albums.album_title, songs.song_name, songs.length FROM albums 
	JOIN songs on albums.id = songs.album_id;

-- d. Joing the 'artists' and 'albums' tables. (find all albums that has a letter a in its name)

SELECT * FROM artists
	JOIN albums on artists.id = albums.artist_id WHERE album_title LIKE "%a%";

-- e. Sort the albums in Z-A order (Show only the first 4 records)

SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;

-- f. Join the 'albums' and 'songs' tables. (Sort albums from Z-A and sort songs from A-Z.)

-- [SECTION] albums Z-A
SELECT * FROM albums
	JOIN songs on albums.id = songs.album_id ORDER BY albums.album_title DESC, songs.song_name ASC;
